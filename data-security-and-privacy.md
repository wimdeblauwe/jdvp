# Jira Database Values Plugin - Data Security and Privacy

This plugin stores no data itself, it only allows to access
data from a database via configuration done via configuration files. It is the responsibility
of whoever configures the plugin for a particular JIRA installation. to ensure there is no 
problem regarding security and privacy.
