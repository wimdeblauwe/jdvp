package org.deblauwe.jira.plugin.databasevalues;

import junit.framework.TestCase;

public class DatabaseRowFormatterTest extends TestCase
{
	public void testFormatDatabaseRow()
	{
		DatabaseRow databaseRow = new DatabaseRow();
		databaseRow.addDatabaseColumn( 0, "Wim" );
		databaseRow.addDatabaseColumn( 1, "Deblauwe" );
		String s = DatabaseRowFormatter.formatDatabaseRow( databaseRow, "{0}", null );
		assertEquals( "Wim", s );
	}

	public void testFormatDatabaseRow2()
	{
		DatabaseRow databaseRow = new DatabaseRow();
		databaseRow.addDatabaseColumn( 0, "Wim" );
		databaseRow.addDatabaseColumn( 1, "Deblauwe" );
		String s = DatabaseRowFormatter.formatDatabaseRow( databaseRow, "{1}, {0}", null );
		assertEquals( "Deblauwe, Wim", s );
	}

	public void testFormatDatabaseRow_JDVP23()
	{
		DatabaseRow databaseRow = new DatabaseRow();
		databaseRow.addDatabaseColumn( 0, "Wim.$" );
		databaseRow.addDatabaseColumn( 1, "Deblauwe" );
		String s = DatabaseRowFormatter.formatDatabaseRow( databaseRow, "{1}, {0}", null );
		assertEquals( "Deblauwe, Wim.$", s );
	}

	public void testFormatDatabaseRow_nullValue()
	{
		DatabaseRow databaseRow = new DatabaseRow();
		databaseRow.addDatabaseColumn( 0, "Wim" );
		databaseRow.addDatabaseColumn( 1, null );
		String s = DatabaseRowFormatter.formatDatabaseRow( databaseRow, "{0} {1}", null );
		assertEquals( "Wim ", s );
	}

	public void testFormatDatabaseRow_customNullValue()
	{
		DatabaseRow databaseRow = new DatabaseRow();
		databaseRow.addDatabaseColumn( 0, "Wim" );
		databaseRow.addDatabaseColumn( 1, null );
		String s = DatabaseRowFormatter.formatDatabaseRow( databaseRow, "{0} {1,N/A}", null );
		assertEquals( "Wim N/A", s );
	}

	public void testFormatDatabaseRow_customNullValue2()
	{
		DatabaseRow databaseRow = new DatabaseRow();
		databaseRow.addDatabaseColumn( 0, "Wim" );
		databaseRow.addDatabaseColumn( 1, null );
		String s = DatabaseRowFormatter.formatDatabaseRow( databaseRow, "{0} {1,Unknown}", null );
		assertEquals( "Wim Unknown", s );
	}

	/**
	 * Unit test for JDVP-39
	 */
	public void testQuoteReplacement()
	{
		DatabaseRow databaseRow = new DatabaseRow();
		databaseRow.addDatabaseColumn( 0, "Tony" ); //extra spaces from db
		databaseRow.addDatabaseColumn( 1, "De'blauwe" ); //extra spaces from db
		String s = DatabaseRowFormatter.formatDatabaseRow( databaseRow, "{1}, {0}", null );
		assertEquals( "De&#39;blauwe, Tony", s );
	}

	/**
	 * Unit test for JDVP-77
	 */
	public void testTrimming()
	{
		DatabaseRow databaseRow = new DatabaseRow();
		databaseRow.addDatabaseColumn( 0, "Wim        " ); //extra spaces from db
		databaseRow.addDatabaseColumn( 1, "  Deblauwe   " ); //extra spaces from db
		String s = DatabaseRowFormatter.formatDatabaseRow( databaseRow, "{1}, {0}", null );
		assertEquals( "Deblauwe, Wim", s );
	}
}
